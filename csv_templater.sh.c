#if 0

THIS_FILE_PATH=$0
THIS_FILE_DIR=${THIS_FILE_PATH%/*}
THIS_FILE=${THIS_FILE_PATH##*/}
EXECUTABLE_NAME=${THIS_FILE%%.*}

#echo this file dir = "'$THIS_FILE_DIR'"
#echo this file path = "'$THIS_FILE_PATH'"
#echo this file = "'$THIS_FILE'"
#echo executable name = "'$EXECUTABLE_NAME'"

cc -std=c99 -I $THIS_FILE_DIR -I $THIS_FILE_DIR/libraries/csv_parser/include $0 -O0 -g3 -o $EXECUTABLE_NAME

./$EXECUTABLE_NAME "$@"

exit
#endif

#include "stdio.h"
#include "stdlib.h"
#include "libraries/csv_parser/include/csvparser.h"
#include "libraries/parg/parg.h"

#define FAILED_ALLOC() fprintf( stderr, "failed allocation: not enough memory\n" ); exit(EXIT_FAILURE)
#define CHECK_ALLOC(a) do{ if ( !a ) { FAILED_ALLOC(); } } while ( 0 )

void
print_usage();

int
copy_with_replacing( const char * in_file, const char * out_file,
                     int to_be_replaced_count,
                     const char ** to_be_replaced,
                     const char ** with );

// TODO(theGiallo, 2017-02-02): check for errors after every fprintf

int
main( int argc, char ** argv )
{
	int ret = 0;
	int i;
	int c;
	struct parg_state ps;
	const char * csv_file_path = NULL;
	const char * template_file_path = NULL;
	CsvParser * csv_parser = NULL;
	const CsvRow * csv_header = NULL;
	const char ** csv_header_fields = NULL;
	int csv_num_fields = 0;
	CsvRow * csv_row = NULL;
	char out_file_name[16];
	char ext[16] = {0};
	int ext_length = 0;
	int template_file_path_length = 0;
	int verbose = 0;

	parg_init( &ps );

	while ( ( c = parg_getopt( &ps, argc, argv, "hvc:s:" ) ) != -1 )
	{
		switch ( c )
		{
			case 1:
				printf( "nonoption '%s'\n", ps.optarg );
			case 'h':
				print_usage();
				return ret;
				break;
			case 'c':
				// printf( "csv: '%s'\n", ps.optarg );
				csv_file_path = ps.optarg;
				break;
			case 's':
				// printf( "template: '%s'\n", ps.optarg );
				template_file_path = ps.optarg;
				break;
			case 'v':
				verbose = 1;
				break;
			case '?':
				if ( ps.optopt == 's' )
				{
					printf( "-s requires an argument\n" );
					return EXIT_FAILURE;
				}
			default:
				print_usage();
				return EXIT_FAILURE;
		}
	}

	if ( !csv_file_path || !template_file_path )
	{
		print_usage();
		return EXIT_FAILURE;
	}

	while ( template_file_path[template_file_path_length] )
	{
		++template_file_path_length;
	}
	for ( i = template_file_path_length - 1;
	      i >= 0 && template_file_path[i] != '.' && ext_length != sizeof( ext );
	      --i )
	{
		++ext_length;
	}
	for ( i = 0; i!=ext_length; ++i )
	{
		ext[i] = template_file_path[template_file_path_length - ext_length + i];
	}

	if ( verbose )
	{
		printf( "going to load data from '%s', using template '%s', with extension '%s'\n",
		        csv_file_path, template_file_path, ext );
	}

	csv_parser = CsvParser_new( csv_file_path, ",", 1 );
	// NOTE(theGiallo): CsvParser_new does not check for malloc result, so
	// either csv_parser is allocated or here we are crashed

	csv_header = CsvParser_getHeader( csv_parser );
	if ( !csv_header )
	{
		fprintf( stderr, "ERROR: %s\n", CsvParser_getErrorMessage( csv_parser ) );
		return EXIT_FAILURE;
	}
	csv_header_fields = CsvParser_getFields( csv_header );
	csv_num_fields = CsvParser_getNumFields( csv_header );

	i = 0;
	while ( ( csv_row = CsvParser_getRow( csv_parser ) ) )
	{
		snprintf( out_file_name, sizeof(out_file_name), "%04d.%s", i, ext );
		if ( -1 == copy_with_replacing( template_file_path, out_file_name,
		                                csv_num_fields,
		                                csv_header_fields,
		                                CsvParser_getFields( csv_row ) ) )
		{
			exit( EXIT_FAILURE );
		}
		CsvParser_destroy_row( csv_row );
		++i;
	}
#if 0
	if ( CsvParser_getErrorMessage( csv_parser ) )
	{
		fprintf( stderr, "ERROR: %s\n", CsvParser_getErrorMessage( csv_parser ) );
		return EXIT_FAILURE;
	}
#endif

	CsvParser_destroy( csv_parser );

	return ret;
}

void
print_usage()
{
	printf(
	   "csv templater by Gianluca Alloisio aka theGiallo\n"
	   "\n"
	   "Usage:   csv_templater -c data_file.csv -s template_file.svg [ -v ]\n"
	   "\n"
	   "   -c specifies the csv file the program will load from. This file first line\n"
	   "      will be treated as an header, containing the strings to substitute for\n"
	   "      each column.\n"
	   "   -s specifies the text file used ad template. Strings contained in the header\n"
	   "      of the csv will be substituted with the data of each row, generating an\n"
	   "      svg for each row of the csv, named with the number of the row, starting\n"
	   "      from 0, padded with zeroes to be 4 chars long.\n"
	   "      Output files will be stored in the current folder.\n"
	   "   -v set verbose mode. Prints some infos.\n"
	);
}

int
copy_with_replacing( const char * in_file, const char * out_file,
                     int to_be_replaced_count,
                     const char ** to_be_replaced,
                     const char ** with )
{
	int ret = 1;
	int i,j;
	int c;
	int max_length = 0;
	int some_matching = 0;
	char * buffer = NULL;
	int buffer_index = 0;
	int replaced = 0;
	int * comparison_indexes = (int*) calloc( to_be_replaced_count, sizeof( int ) );
	int * to_be_replaced_str_lengths = (int*) calloc( to_be_replaced_count, sizeof( int ) );
	CHECK_ALLOC( comparison_indexes );
	CHECK_ALLOC( to_be_replaced_str_lengths );
	for ( i = 0; i != to_be_replaced_count; ++i )
	{
		for( j = 0; to_be_replaced[i][j]; ++j )
		{
			++to_be_replaced_str_lengths[i];
		}
		if ( max_length < to_be_replaced_str_lengths[i] )
		{
			max_length = to_be_replaced_str_lengths[i];
		}
	}
	buffer = (char*) calloc( max_length + 1, sizeof(char) );
	CHECK_ALLOC( buffer );

	#if 0
		printf( "should substitute in file '%s' to file '%s'\n"
		        "%d values:\n",
		        in_file, out_file, to_be_replaced_count );
	for ( i = 0; i != to_be_replaced_count; ++i )
	{
		printf( "   %s\n", to_be_replaced[i] );
	}
	printf( "with:\n" );
	for ( i = 0; i != to_be_replaced_count; ++i )
	{
		printf( "   %s\n", with[i] );
	}
	#endif

	FILE * in_fp  = fopen( in_file, "r" );
	if ( !in_fp )
	{
		ret = 0;
		fprintf( stderr, "could not open input file '%s'\n", in_file );
		return ret;
	}
	FILE * out_fp = fopen( out_file, "w" );
	if ( !out_fp )
	{
		ret = 0;
		fprintf( stderr, "could not open output file '%s'\n", out_file );
		return ret;
	}

	while( ( c = fgetc( in_fp ) ) != EOF )
	{
		some_matching = 0;
		for ( i = 0; i != to_be_replaced_count; ++i )
		{
			if ( c == to_be_replaced[i][comparison_indexes[i]] )
			{
				++comparison_indexes[i];
				some_matching = 1;
				if ( comparison_indexes[i] == to_be_replaced_str_lengths[i] )
				{
					buffer_index = 0;
					replaced = 1;
					fprintf( out_fp, "%s", with[i] );
					for ( j = 0; j != to_be_replaced_count; ++j )
					{
						comparison_indexes[j] = 0;
					}
					break;
				}
			} else
			{
				comparison_indexes[i] = 0;
			}
		}
		if ( !replaced )
		{
			buffer[buffer_index] = c;
			++buffer_index;
			if ( !some_matching )
			{
				buffer[buffer_index] = 0;
				fprintf( out_fp, "%s", buffer );
				buffer_index = 0;
			}
		}
		replaced = 0;
	}
	if ( ferror( in_fp ) )
	{
		fprintf( stderr, "there was an error reading input file '%s'\n", in_file );
		perror( "" );
		return 0;
	} else
	if ( buffer_index )
	{
		buffer[buffer_index+1] = 0;
		fprintf( out_fp, "%s", buffer );
		buffer_index = 0;
	}

	fclose( in_fp );
	fclose( out_fp );

	return ret;
}


#include "libraries/csv_parser/src/csvparser.c"
#include "libraries/parg/parg.c"
