# csv_templater

    Filling a text template with data from a csv, generating a file for each row.

## Examples of uses
 - in-design like templating using Inkskape. Just create an svg and write `@text`
   instead of text values, modify the .svg with a text editor and replace colours
   or image paths with `@something`. Write your `@stuff` in the first row of your
   csv and give it to csv_templater with the modified svg.
 - generate static HTML pages from data
 - write personalized emails or invites

## Practical Example

CSV:
`friends.csv`
```
@name,@surname
John,Smith
Rose,Johnson
```

template:
`template.txt`
```
Hi @name,

I am glad to personally write you to invite you to my birthday party!
It will be the obvious day at my house, for dinner, 20pm.

Hoping to see you there!
Jake
```

command: `csv_templater.sh.c -c friends.csv -t template.txt`

resulting files:

`0000.txt`
```
Hi John,

I am glad to personally write you to invite you to my birthday party!
It will be the obvious day at my house, for dinner, 20pm.

Hoping to see you there!
Jake
```

`0001.txt`
```
Hi Rose,

I am glad to personally write you to invite you to my birthday party!
It will be the obvious day at my house, for dinner, 20pm.

Hoping to see you there!
Jake
```

## Usage

```
csv templater by Gianluca Alloisio aka theGiallo

Usage:   csv_templater -c data_file.csv -s template_file.svg [ -v ]

   -c specifies the csv file the program will load from. This file first line
      will be treated as an header, containing the strings to substitute for
      each column.
   -s specifies the text file used ad template. Strings contained in the header
      of the csv will be substituted with the data of each row, generating an
      svg for each row of the csv, named with the number of the row, starting
      from 0, padded with zeroes to be 4 chars long.
      Output files will be stored in the current folder.
   -v set verbose mode. Prints some infos.
```
